//
//  SnowFlake.swift
//  SnowFlake
//
//  Created by David van Enckevort on 08/12/2018.
//  Copyright © 2018 All Things Digital. All rights reserved.
//
import Foundation
import os

public final class SnowFlake: Sequence, IteratorProtocol {
    public typealias Element = Int64

    public enum SnowFlakeError: Error {
        case workerOutOfBounds, datacenterOutOfBounds
    }

    /// Standard epoch for the timestamp of a SnowFlake id
    public static let twitterEpoch = Date(timeIntervalSince1970: 1288834974.657)
    /// Number of bits in the worker id.
    public static let workerIdBits = 5
    /// Number of bits in the datacenter id
    public static let datacenterIdBits = 5
    /// Number of bits in the sequence number of the id
    public static let sequenceBits = 12
    /// Shift for the timestamp in the id
    public static let timestampLeftShift = workerIdBits + datacenterIdBits + sequenceBits
    /// Maximum value for the worker id
    public static let maxWorkerId = -1 ^ (-1 << workerIdBits)
    /// Maximum value for the datacenter id
    public static let maxDatacenterId = -1 ^ (-1 << datacenterIdBits)
    /// Mask for the sequence
    public static let sequenceMask = Int64(-1 ^ (-1 << sequenceBits))

    /// Worker id
    private let worker: Int64
    /// Datacenter id
    private let datacenter: Int64
    /// Epoch on which the timestamp is based
    private let epoch: Date
    /// Lock used for synchronisation
    private let lock: NSLock
    /// Timestamp of the last id generated
    private var lastTimestamp: Int64
    /// Sequence number for the id's generated within the same millisecond
    private var sequence: Int64

    /// Create a new Snowflake sequence generator
    ///
    /// - Parameters:
    ///   - worker: id of the worker in the range 0...SnowFlake.maxWorkerId (default 0)
    ///   - datacenter: id of the datacenter in the range 0..<SnowFlake.maxDatacenterId (default 0)
    ///   - sequence: initial value for the sequence (default 0)
    ///   - epoch: start date of the epoch on which the timestamp is based, default is the Twitter epoch
    /// - Throws: A SnowFlakeError.workerOutOfBounds if the worker is out of bounds, A SnowFlakeError.datacenterOutOfBounds if the datacenter is out of bounds.
    public init(worker: Int = 0, datacenter: Int = 0, sequence: Int64 = 0, epoch: Date = SnowFlake.twitterEpoch) throws {
        guard (0...SnowFlake.maxWorkerId).contains(worker) else { throw SnowFlakeError.workerOutOfBounds }
        guard (0...SnowFlake.maxDatacenterId).contains(datacenter) else { throw SnowFlakeError.datacenterOutOfBounds }
        self.datacenter = Int64(datacenter) << (SnowFlake.sequenceBits + SnowFlake.datacenterIdBits)
        self.worker = Int64(worker) << SnowFlake.sequenceBits
        self.sequence = sequence
        self.epoch = epoch
        self.lock = NSLock()
        self.lastTimestamp = -1
    }

    /// Generate a new id. The id is structured as follows.
    /// 63-22 timestamp
    /// 21-17 worker id
    /// 16-12 datacenter id
    /// 11-0  increment
    ///
    /// - Returns: The generated id
    public func next() -> Int64? {
        lock.lock()
        defer { lock.unlock() }
        var timestamp = timeGen()
        guard timestamp >= lastTimestamp else {
            os_log("Clock moved backwards. Refusing to generate id for %@ milliseconds", lastTimestamp - timestamp)
            return nil
        }
        if timestamp == lastTimestamp {
            sequence = sequence + 1 & SnowFlake.sequenceMask
            if sequence == 0 {
                timestamp = tilNextMillis(lastTimestamp)
            }
        } else {
            sequence = 0
        }
        lastTimestamp = timestamp
        return (lastTimestamp << SnowFlake.timestampLeftShift) | datacenter | worker | sequence
    }


    /// Spin wait until the next timestamp. This function spin through a loop
    /// until the system time is past the given timestamp.
    ///
    /// - Parameter lastTimestamp: previous timestamp
    /// - Returns: new timestamp.
    private func tilNextMillis(_ lastTimestamp: Int64) -> Int64 {
        var timestamp = timeGen()
        while timestamp <= lastTimestamp {
            timestamp = timeGen()
        }
        return timestamp
    }

    /// Get a new timestamp in milliseconds based on the system clock.
    ///
    /// - Returns: The timestamp in milliseconds based on the system clock.
    private func timeGen() -> Int64 {
        return Int64(Date().timeIntervalSince(epoch) * 1000)
    }

}
