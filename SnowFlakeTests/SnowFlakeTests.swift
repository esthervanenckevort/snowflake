//
//  SnowFlakeTests.swift
//  SnowFlakeTests
//
//  Created by David van Enckevort on 08/12/2018.
//  Copyright © 2018 All Things Digital. All rights reserved.
//
@testable import SnowFlake
import XCTest

class SnowFlakeTests: XCTestCase {
    var snowflake: SnowFlake!

    override func setUp() {
        snowflake = try! SnowFlake()
    }

    func testInitWorkerOutofBounds() {
        do {
            _ = try SnowFlake(worker: 32, datacenter: 0)
            XCTFail()
        } catch SnowFlake.SnowFlakeError.workerOutOfBounds {
        } catch {
            XCTFail()
        }
    }

    func testGenerateAccurateTimestamp() {
        let testStart = Date()
        guard let flake = snowflake.next() else {
            XCTFail()
            return
        }
        let millisecondsSinceEpoch = (flake >> 22)
        let millisecondsSince1970 = millisecondsSinceEpoch + 1288834974657
        let timeIntervalSince1970 = Double(millisecondsSince1970) / 1000
        let timestamp = Date(timeIntervalSince1970: timeIntervalSince1970)
        let difference = timestamp.timeIntervalSince(testStart)
        XCTAssert((-0.001...0.001).contains(difference), "Time should be within rounding margins the same as the start time.")
    }

    func testCorrectWorkerId() {
        let workerMask: Int64 = 0x000000000001F000
        for id in 0...SnowFlake.maxWorkerId {
            let snowflake = try! SnowFlake(worker: id)
            let flake = snowflake.next()!
            let worker = (flake & workerMask) >> 12
            XCTAssert(worker == id, "Worker id should be preserved in the id - is \(worker)")
        }
    }

    func testCorrectDataCenterId() {
        let datacenterMask: Int64 = 0x00000000003E0000
        for id in 0...SnowFlake.maxDatacenterId {
            let snowflake = try! SnowFlake(datacenter: id)
            let flake = snowflake.next()!
            let datacenter = (flake & datacenterMask) >> 17
            XCTAssert(datacenter == id, "Datacenter id should be preserved in the id - is \(datacenter)")
        }
    }

    func testRollOverSequence() {
        for _ in 0..<10_000 {
            guard let flake = snowflake.next() else { continue }
            XCTAssertFalse(flake & 0x1000 == 0x1000, "Sequence should not overwrite the worker id")
        }
    }

    func testGenerateOne() {
        let minId: Int64 = 50000000000
        guard let id = snowflake.next() else {
            XCTFail()
            return
        }
        XCTAssert(id > minId, "Id should be larger than the minimal id \(minId) - is \(id)")
    }

    func testGenerateUnique() {
        let rounds = 1_000_000
        let snowflake = try! SnowFlake()
        var results = Set<Int64>()
        for _ in 0..<rounds {
            guard let flake = snowflake.next() else {
                XCTFail()
                continue
            }
            results.insert(flake)
        }
        XCTAssert(results.count == rounds, "Number of unique id's should be \(rounds) - is \(results.count)")
    }

    func testPerformance() {
        let rounds = 1_000_000
        let snowflake = try! SnowFlake(worker: 0, datacenter: 0)
        self.measure {
            for _ in 0...rounds {
                _ = snowflake.next()
            }
        }
    }

}
